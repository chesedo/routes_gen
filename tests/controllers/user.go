package controllers

import "errors"

type UserModel struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}

type UserController struct {
	users   []UserModel
	last_id int
}

func NewUserController() UserController {
	return UserController{
		users:   []UserModel{{Id: 0, Name: "John"}, {Id: 2, Name: "Susan"}},
		last_id: 2,
	}
}

func (c *UserController) GetAll() (*[]UserModel, error) {
	return &c.users, nil
}

func (c *UserController) Get(id int) (*UserModel, error) {
	for _, u := range c.users {
		if u.Id == id {
			return &u, nil
		}
	}

	return nil, errors.New("User not found")
}

func (c *UserController) Add(user UserModel) error {
	c.last_id++
	user.Id = c.last_id

	c.users = append(c.users, user)

	return nil
}
