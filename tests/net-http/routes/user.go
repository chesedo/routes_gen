package routes

import (
	"encoding/json"
	"net/http"
	"strconv"

	"gitlab.com/chesedo/routes_gen/tests/controllers"
)

func RegisterUsers(mux *http.ServeMux) {
	controller := controllers.NewUserController()

	mux.HandleFunc("/user", handleUser(controller))
}

func handleUser(controller controllers.UserController) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		switch r.Method {
		case "GET":
			handleUserGet(w, r, controller)
		case "POST":
			handleUserPost(w, r, controller)
		default:
			handleUserDefault(w, r, controller)
		}
	}
}

func handleUserGet(w http.ResponseWriter, r *http.Request, controller controllers.UserController) {
	id_str := r.FormValue("id")

	if id_str == "" {
		response, err := controller.GetAll()

		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(w).Encode(struct {
				Message string `json:"message"`
			}{Message: err.Error()})

			return
		}

		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(response)
		return
	}

	id, err := strconv.Atoi(id_str)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(struct {
			Message string `json:"message"`
		}{Message: "failed to convert `id` to int"})

		return
	}
	response, err := controller.Get(id)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(struct {
			Message string `json:"message"`
		}{Message: err.Error()})

		return
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(response)
}

func handleUserPost(w http.ResponseWriter, r *http.Request, controller controllers.UserController) {
	var user controllers.UserModel

	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(struct {
			Message string `json:"message"`
		}{Message: err.Error()})

		return
	}

	if user.Name == "" {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(struct {
			Message string `json:"message"`
		}{Message: "Name is required"})
		return
	}
	err = controller.Add(user)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(struct {
			Message string `json:"message"`
		}{Message: err.Error()})

		return
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(struct {
		Message string `json:"message"`
	}{Message: "Added"})
}

func handleUserDefault(w http.ResponseWriter, r *http.Request, controller controllers.UserController) {
	w.Header().Set("Allow", "GET")
	w.WriteHeader(http.StatusMethodNotAllowed)

	json.NewEncoder(w).Encode(struct {
		Message string `json:"message"`
	}{Message: "Method not supported"})
}
