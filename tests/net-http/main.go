package main

import (
	"log"
	"net/http"

	"gitlab.com/chesedo/routes_gen/tests/net-http/routes"
)

func main() {
	mux := http.NewServeMux()
	routes.RegisterUsers(mux)
	log.Println("Server started on http://localhost:8080")
	log.Fatal(http.ListenAndServe(":8080", mux))
}
