FROM golang:alpine AS base


FROM base AS libraries


# Run tests
FROM libraries as test
CMD go test


## Build binary
FROM libraries AS build
RUN go build
