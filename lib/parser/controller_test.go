package parser

import (
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"reflect"
	"testing"
)

func Test_parseControllerDecl(t *testing.T) {
	cases := []struct {
		description string
		genDecl     *ast.GenDecl
		controller  *Controller
	}{
		{"Not a controller", getGenDecl("var name string"), nil},
		{
			"Interface",
			getGenDecl("type User interface {}"),
			nil,
		},
		{
			"Controller with no comments",
			getGenDecl("type User struct {}"),
			&Controller{Controller: "User"},
		},
		{
			"Controller with path",
			getGenDecl(`
        // @Path /user
        type User struct {}
      `),
			&Controller{Path: "/user", Controller: "User"},
		},
		{
			"Controller with path parameters",
			getGenDecl(`
        // @Path /{type}/{id}
        type User struct {}
      `),
			&Controller{Path: "/{type}/{id}", Controller: "User", Parameters: []*Parameter{{Name: "type", Source: SourcePath, Position: -1}, {Name: "id", Source: SourcePath, Position: -1}}},
		},
		{
			"Use single",
			getGenDecl(`
        // @Use Authorization
        type Product struct {}
      `),
			&Controller{Controller: "Product", Uses: []string{"Authorization"}},
		},
		{
			"Use multiple",
			getGenDecl(`
        // @Use Authorization
        // @Use Logger
        type User struct {}
      `),
			&Controller{Controller: "User", Uses: []string{"Authorization", "Logger"}},
		},
		{
			"Param documentation",
			getGenDecl(`
        // @Path /user/{id}
        // @Param id Identifier of the user to operate against
        type User struct {}
      `),
			&Controller{Path: "/user/{id}", Controller: "User", Parameters: []*Parameter{{Name: "id", Description: "Identifier of the user to operate against", Source: SourcePath, Position: -1}}},
		},
		{
			"Param documentation before route",
			getGenDecl(`
        // @Param id Identifier of the user to operate against
        // @Path /user/{id}
        type User struct {}
      `),
			&Controller{Path: "/user/{id}", Controller: "User", Parameters: []*Parameter{{Name: "id", Description: "Identifier of the user to operate against", Source: SourcePath, Position: -1}}},
		},
	}

	for _, test := range cases {
		actual := parseControllerDecl(test.genDecl)

		if !reflect.DeepEqual(actual, test.controller) {
			t.Errorf("'%s' controllers do not match: %s", test.description, diff(actual, test.controller))
		}
	}
}

func getGenDecl(x string) *ast.GenDecl {
	x = fmt.Sprintf(`package test
	  %s
	  `, x)

	fset := token.NewFileSet()

	e, err := parser.ParseFile(fset, "", x, parser.ParseComments)

	if err != nil {
		panic(err)
	}

	return e.Decls[0].(*ast.GenDecl)
}
