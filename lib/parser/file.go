package parser

import (
	"go/ast"
	"go/token"
)

func parseFile(f *ast.File) *Controller {
	if len(f.Decls) == 0 {
		return nil
	}

	var controller *Controller
	routes := []*Route{}
	possibleConstructors := map[string][]injector{}

	for _, node := range f.Decls {
		if genD, ok := node.(*ast.GenDecl); ok && genD.Tok == token.TYPE {
			tmp := parseControllerDecl(genD)

			if tmp != nil {
				controller = tmp

				continue
			}
		}
		if fn, ok := node.(*ast.FuncDecl); ok {
			tmp := parseFuncDecl(fn)

			if tmp != nil {
				routes = append(routes, tmp)

				continue
			}

			tmpInjectors, returnType := parseConstructorDecl(fn)
			possibleConstructors[returnType] = tmpInjectors
		}
	}

	if len(routes) > 0 {
		controller.Routes = routes
	}

	if injectors, ok := possibleConstructors[controller.Controller]; ok {
		controller.Injectors = injectors
	}

	return controller
}
