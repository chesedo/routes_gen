package parser

import (
	"go/ast"
	"strings"
)

type Controller struct {
	Controller string
	Path       string
	Parameters []*Parameter
	Uses       []string
	Routes     []*Route
	Injectors  []injector
}

func parseControllerDecl(decl *ast.GenDecl) *Controller {
	typeSpec, ok := isStruct(decl)
	if !ok {
		return nil
	}

	c := Controller{Controller: typeSpec.Name.Name}

	if decl.Doc == nil {
		return &c
	}

	parseControllerComments(&c, decl)

	return &c
}

func isStruct(decl *ast.GenDecl) (*ast.TypeSpec, bool) {
	typeSpec, ok := decl.Specs[0].(*ast.TypeSpec)
	if !ok {
		return nil, false
	}
	_, ok = typeSpec.Type.(*ast.StructType)
	if !ok {
		return nil, false
	}

	return typeSpec, true
}

func parseControllerComments(controller *Controller, decl *ast.GenDecl) {
	tokens := make(chan *tok)
	go tokenizeControllerComment(decl.Doc.List, tokens)

	for token := range tokens {
		switch token.identifier {
		case tokenPath:
			controller.Path = token.value
			params := getParametersFromPath(controller.Path)
			mergeParameters(&controller.Parameters, params)
		case tokenUse:
			controller.Uses = append(controller.Uses, token.value)
		case tokenParam:
			name, description := getNameAndDescription(token.value)
			mergeParameter(&controller.Parameters, &Parameter{Name: name, Description: description, Position: -1})
		}
	}
}

func tokenizeControllerComment(comments []*ast.Comment, channel chan<- *tok) {
	for _, comment := range comments {
		line := strings.TrimPrefix(comment.Text, "// ")
		pos := strings.Index(line, " ")

		if pos < 0 {
			continue
		}

		key := line[:pos]
		value := strings.TrimSpace(line[pos+1:])

		switch key {
		case "@Path":
			channel <- &tok{tokenPath, value}
		case "@Use":
			channel <- &tok{tokenUse, value}
		case "@Param":
			channel <- &tok{tokenParam, value}
		}
	}

	close(channel)
}
