package parser

import (
	"go/ast"
	"strings"
)

type Method string

const (
	Get    = Method("GET")
	Post   = Method("POST")
	Put    = Method("PUT")
	Delete = Method("DELETE")
)

type Returns int

const (
	Response Returns = iota
	Error
)

type Route struct {
	Description string
	Path        string
	Method      Method
	Function    string
	Parameters  []*Parameter
	Returns     []Returns
	Accepts     string
	Produce     string
	Uses        []string
}

func parseFuncDecl(decl *ast.FuncDecl) *Route {
	if !isMethod(decl) {
		return nil
	}

	r := Route{Function: decl.Name.Name}

	if decl.Doc == nil {
		return &r
	}

	parseMethodComments(&r, decl)

	if decl.Type.Results != nil {
		res := decl.Type.Results.List
		r.Returns = make([]Returns, 0, len(res))

		for _, re := range res {
			if typ, ok := re.Type.(*ast.Ident); ok && typ.Name == "error" {
				r.Returns = append(r.Returns, Error)
			} else {
				r.Returns = append(r.Returns, Response)
			}
		}
	}

	return &r
}

func isMethod(decl *ast.FuncDecl) bool {
	return decl.Recv != nil
}

func parseMethodComments(route *Route, decl *ast.FuncDecl) {
	tokens := make(chan *tok)
	go tokenizeMethodComment(decl.Doc.List, tokens)
	descriptions := []string{}

	for token := range tokens {
		switch token.identifier {
		case tokenDescription:
			descriptions = append(descriptions, token.value)
		case tokenPath:
			route.Path = token.value
			params := getParametersFromPath(route.Path)
			mergeParameters(&route.Parameters, params)
		case tokenHttpMethod:
			route.Method = Method(token.value)
		case tokenAccepts:
			route.Accepts = token.value
		case tokenProduces:
			route.Produce = token.value
		case tokenUse:
			route.Uses = append(route.Uses, token.value)
		case tokenBody:
			name, description := getNameAndDescription(token.value)
			route.Parameters = append(route.Parameters, &Parameter{Source: SourceBody, Name: name, Description: description, Position: -1})
		case tokenParam:
			name, description := getNameAndDescription(token.value)
			mergeParameter(&route.Parameters, &Parameter{Name: name, Description: description, Position: -1})
		case tokenQuery:
			name, description := getNameAndDescription(token.value)
			route.Parameters = append(route.Parameters, &Parameter{Source: SourceQuery, Name: name, Description: description, Position: -1})
		}
	}

	enrichParametersUsingFuncFields(route.Parameters, decl.Type.Params.List)
	route.Description = strings.Join(descriptions, " ")
}

func tokenizeMethodComment(comments []*ast.Comment, channel chan<- *tok) {
	for _, comment := range comments {
		line := strings.TrimPrefix(comment.Text, "// ")
		pos := strings.Index(line, " ")

		if pos < 0 {
			tokenizeMethodCommentKeyOnly(line, channel)
			continue
		}

		tokenizeMethodCommentKeyAndValue(line, pos, channel)
	}

	close(channel)
}

func tokenizeMethodCommentKeyOnly(line string, channel chan<- *tok) {
	if line[0] != '@' {
		channel <- &tok{tokenDescription, line}
	} else if line == "@Get" {
		channel <- &tok{tokenHttpMethod, string(Get)}
	} else if line == "@Post" {
		channel <- &tok{tokenHttpMethod, string(Post)}
	} else if line == "@Put" {
		channel <- &tok{tokenHttpMethod, string(Put)}
	} else if line == "@Delete" {
		channel <- &tok{tokenHttpMethod, string(Delete)}
	}
}

func tokenizeMethodCommentKeyAndValue(line string, pos int, channel chan<- *tok) {
	key := line[:pos]
	value := strings.TrimSpace(line[pos+1:])

	switch key {
	case "@Path":
		channel <- &tok{tokenPath, value}
	case "@Post":
		channel <- &tok{tokenHttpMethod, string(Post)}
		channel <- &tok{tokenPath, value}
	case "@Get":
		channel <- &tok{tokenHttpMethod, string(Get)}
		channel <- &tok{tokenPath, value}
	case "@Put":
		channel <- &tok{tokenHttpMethod, string(Put)}
		channel <- &tok{tokenPath, value}
	case "@Delete":
		channel <- &tok{tokenHttpMethod, string(Delete)}
		channel <- &tok{tokenPath, value}
	case "@Accepts":
		channel <- &tok{tokenAccepts, value}
	case "@Produces":
		channel <- &tok{tokenProduces, value}
	case "@Use":
		channel <- &tok{tokenUse, value}
	case "@Body":
		channel <- &tok{tokenBody, value}
	case "@Param":
		channel <- &tok{tokenParam, value}
	case "@Query":
		channel <- &tok{tokenQuery, value}
	default:
		if key[0] == '@' {
			break
		}
		channel <- &tok{tokenDescription, line}
	}
}

type parameterSource int

const (
	SourcePath parameterSource = iota
	SourceBody
	SourceQuery
)

type Parameter struct {
	Name        string
	Description string
	Source      parameterSource
	Position    int
	Path        string
	Type        string
	IsRef       bool
}

func enrichParametersUsingFuncFields(parameters []*Parameter, fields []*ast.Field) {
	i := 0
	for _, field := range fields {
		for _, name := range field.Names {
			var path, typ string
			var isRef bool
			expr := field.Type

			if star, ok := expr.(*ast.StarExpr); ok {
				expr = star.X
				isRef = true
			}

			if ident, ok := expr.(*ast.Ident); ok {
				typ = ident.Name
			} else if selector, ok := expr.(*ast.SelectorExpr); ok {
				x := selector.X.(*ast.Ident)
				path = x.Name
				typ = selector.Sel.Name
			}

			for _, param := range parameters {
				if name.Name == param.Name {
					param.Position = i
					param.Type = typ
					param.Path = path
					param.IsRef = isRef
					continue // Go to next field name as an optimization
				}
			}

			i++
		}
	}
}
