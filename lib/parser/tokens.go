// This file provides all the tokens used by parsers
package parser

type tok struct {
	identifier tokenIdentifier
	value      string
}

type tokenIdentifier int

const (
	tokenPath tokenIdentifier = iota
	tokenUse
	tokenDescription
	tokenHttpMethod
	tokenAccepts
	tokenProduces
	tokenBody
	tokenParam
	tokenQuery
)
