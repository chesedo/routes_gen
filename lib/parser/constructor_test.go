package parser

import (
	"go/ast"
	"reflect"
	"testing"
)

func Test_parseConstructorDecl(t *testing.T) {
	cases := []struct {
		description string
		funcDecl    *ast.FuncDecl
		injectors   []injector
		returnType  string
	}{
		{"Method function", getFuncDecl("func (c *controlller) add(){}"), nil, ""},
		{"Not a constructor function", getFuncDecl("func add(){}"), nil, ""},
		{"Missing return type", getFuncDecl("func NewController(){}"), nil, ""},
		{"Simple constructor wrong return type", getFuncDecl("func NewController() User {}"), nil, "User"},
		{
			"Complex constructor",
			getFuncDecl("func NewController(max int, repo string) controller {}"),
			[]injector{
				{name: "max", Type: "int"},
				{name: "repo", Type: "string"},
			},
			"controller",
		},
		{
			"Constructor with compound types",
			getFuncDecl("func NewController(min, max int) controller {}"),
			[]injector{
				{name: "min", Type: "int"},
				{name: "max", Type: "int"},
			},
			"controller",
		},
	}

	for _, c := range cases {
		actual, returnType := parseConstructorDecl(c.funcDecl)

		if !reflect.DeepEqual(actual, c.injectors) {
			t.Errorf("'%s' injectors do not match: %s", c.description, diff(actual, c.injectors))
		}

		if returnType != c.returnType {
			t.Errorf("'%s' return types do not match: %s != %s", c.description, returnType, c.returnType)
		}
	}
}
