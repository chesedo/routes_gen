package parser

import (
	"go/ast"
	"strings"
)

type injector struct {
	name string
	Type string
}

func parseConstructorDecl(decl *ast.FuncDecl) (injectors []injector, returnType string) {
	if isMethod(decl) {
		return
	}

	funcName := decl.Name.Name
	if funcName[:3] != "New" {
		return
	}

	if decl.Type.Results == nil {
		return
	}

	returnIdent, ok := decl.Type.Results.List[0].Type.(*ast.Ident)
	if !ok {
		return
	}

	returnType = returnIdent.Name
	titleCaseReturnType := strings.Title(returnType)
	typeFromConstructor := funcName[3:]

	if typeFromConstructor != titleCaseReturnType {
		return
	}

	params := decl.Type.Params.List
	injectors = make([]injector, 0, len(params))

	for _, param := range params {
		ident, ok := param.Type.(*ast.Ident)
		if !ok {
			continue
		}
		typ := ident.Name

		for _, name := range param.Names {
			injectors = append(injectors, injector{name: name.Name, Type: typ})
		}
	}

	return
}
