package parser

import "strings"

func getParametersFromPath(path string) []*Parameter {
	var params []*Parameter
	inBrace := false
	openPos := 0

	for pos, char := range path {
		if char == '{' && !inBrace {
			inBrace = true
			openPos = pos
		} else if char == '}' && inBrace {
			inBrace = false
			params = append(params, &Parameter{Name: path[openPos+1 : pos], Source: SourcePath, Position: -1})
		}
	}

	return params
}

func getNameAndDescription(value string) (name string, description string) {
	pos := strings.Index(value, " ")

	if pos > 0 {
		name = value[:pos]
		description = value[pos+1:]
	} else {
		name = value
	}

	return
}

func mergeParameters(excisting *[]*Parameter, params []*Parameter) {
	for _, param := range params {
		mergeParameter(excisting, param)
	}
}

func mergeParameter(excisting *[]*Parameter, param *Parameter) {
	// Update existing parameter
	for _, p := range *excisting {
		if p.Name == param.Name {
			if p.Description == "" {
				p.Description = param.Description
			}
			return
		}
	}

	// Else add when not already in parameters
	*excisting = append(*excisting, param)
}
