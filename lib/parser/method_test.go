package parser

import (
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"reflect"
	"testing"

	"github.com/davecgh/go-spew/spew"
	"github.com/sergi/go-diff/diffmatchpatch"
)

func Test_parseFuncDecl(t *testing.T) {
	cases := []struct {
		description string
		funcDecl    *ast.FuncDecl
		route       *Route
	}{
		{"Not a method function", getFuncDecl("func add(){}"), nil},
		{"Method with no comments", getFuncDecl("func (c *controller) add(){}"), &Route{Function: "add"}},
		{
			"Method with comment",
			getFuncDecl(`
        // Add a new item
        func (c *controller) add(){}
      `),
			&Route{Description: "Add a new item", Function: "add"},
		},
		{
			"Method with comment - one word",
			getFuncDecl(`
        // World
        func (c *controller) hello(){}
      `),
			&Route{Description: "World", Function: "hello"},
		},
		{
			"Method with multiline comment",
			getFuncDecl(`
        // Add a new item.
        // Will fail if the item already exists.
        func (c *controller) add(){}
      `),
			&Route{Description: "Add a new item. Will fail if the item already exists.", Function: "add"},
		},
		{
			"Method with multiple comments",
			getFuncDecl(`
        // Add a new item
        // @Post /item
        func (c *controller) add(){}
      `),
			&Route{Description: "Add a new item", Path: "/item", Method: Post, Function: "add"},
		},
		{
			"Method with parameters",
			getFuncDecl(`
        // @Path /{id}/{page}
        func (c *controller) get(id int){}
      `),
			&Route{Path: "/{id}/{page}", Function: "get", Parameters: []*Parameter{{Name: "id", Source: SourcePath, Position: 0, Type: "int"}, {Name: "page", Source: SourcePath, Position: -1}}},
		},
		{
			"Path comment",
			getFuncDecl(`
        // @Path /contact
        func (c *controller) contact(){}
      `),
			&Route{Path: "/contact", Function: "contact"},
		},
		{
			"Get method",
			getFuncDecl(`
        // @Get
        func (c *controller) All(){}
      `),
			&Route{Path: "", Function: "All", Method: Get},
		},
		{
			"Get method with parameters",
			getFuncDecl(`
        // @Get /{page}/{limit}
        func (c *controller) products(page, limit int){}
      `),
			&Route{Path: "/{page}/{limit}", Function: "products", Method: Get, Parameters: []*Parameter{{Name: "page", Source: SourcePath, Position: 0, Type: "int"}, {Name: "limit", Source: SourcePath, Position: 1, Type: "int"}}},
		},
		{
			"Get method with complex path and parameters out of order",
			getFuncDecl(`
        // @Get /from/{from}/to/{to}/filter/{filter}
        func (c *controller) events(filter string, from, to time.Time){}
      `),
			&Route{Path: "/from/{from}/to/{to}/filter/{filter}", Function: "events", Method: Get, Parameters: []*Parameter{{Name: "from", Source: SourcePath, Position: 1, Path: "time", Type: "Time"}, {Name: "to", Source: SourcePath, Position: 2, Path: "time", Type: "Time"}, {Name: "filter", Source: SourcePath, Position: 0, Type: "string"}}},
		},
		{
			"Post method with error return type",
			getFuncDecl(`
        // @Post
        func (c *controller) Insert() error {}
      `),
			&Route{Path: "", Function: "Insert", Method: Post, Returns: []Returns{Error}},
		},
		{
			"Get method with response and error return type",
			getFuncDecl(`
        // @Get
        func (c *controller) Get() (User, error) {}
      `),
			&Route{Path: "", Function: "Get", Method: Get, Returns: []Returns{Response, Error}},
		},
		{
			"Post method",
			getFuncDecl(`
        // @Post
        func (c *controller) insert(){}
      `),
			&Route{Path: "", Function: "insert", Method: Post},
		},
		{
			"Post method path",
			getFuncDecl(`
        // @Post /product
        // @Body product The product to insert
        func (c *controller) Insert(product *Product){}
      `),
			&Route{Path: "/product", Function: "Insert", Method: Post, Parameters: []*Parameter{{Name: "product", Description: "The product to insert", Source: SourceBody, Position: 0, Type: "Product", IsRef: true}}},
		},
		{
			"Put method",
			getFuncDecl(`
        // @Put
        func (c *controller) Update(){}
      `),
			&Route{Path: "", Function: "Update", Method: Put},
		},
		{
			"Put method path",
			getFuncDecl(`
        // @Put /category
        // @Body category The category to update
        func (c *controller) update(category *Category){}
      `),
			&Route{Path: "/category", Function: "update", Method: Put, Parameters: []*Parameter{{Name: "category", Description: "The category to update", Source: SourceBody, Position: 0, Type: "Category", IsRef: true}}},
		},
		{
			"Delete method",
			getFuncDecl(`
        // @Delete
        func (c *controller) remove(){}
      `),
			&Route{Path: "", Function: "remove", Method: Delete},
		},
		{
			"Delete method path parameters",
			getFuncDecl(`
        // @Delete /user/{userType}
        // @Body user
        func (c *controller) Delete(user *User, userType string){}
      `),
			&Route{Path: "/user/{userType}", Function: "Delete", Method: Delete, Parameters: []*Parameter{{Name: "userType", Source: SourcePath, Position: 1, Type: "string"}, {Name: "user", Source: SourceBody, Position: 0, Type: "User", IsRef: true}}},
		},
		{
			"Accepts",
			getFuncDecl(`
        // @Accepts application/json
        // @Body filter
        func (c *controller) find(filter *Filter){}
      `),
			&Route{Function: "find", Accepts: "application/json", Parameters: []*Parameter{{Name: "filter", Source: SourceBody, Position: 0, Type: "Filter", IsRef: true}}},
		},
		{
			"Produces",
			getFuncDecl(`
        // @Produces application/xml
        func (c *controller) products() []*Product {}
      `),
			&Route{Function: "products", Produce: "application/xml", Returns: []Returns{Response}},
		},
		{
			"Unknown annotation",
			getFuncDecl(`
        // @Route
        func (c *controller) get() {}
      `),
			&Route{Function: "get"},
		},
		{
			"Unknown annotation with value",
			getFuncDecl(`
        // @Route /somewhere/over/the/ocean
        func (c *controller) get() {}
      `),
			&Route{Function: "get"},
		},
		{
			"Use single",
			getFuncDecl(`
        // @Use Authorization
        func (c *controller) delete() {}
      `),
			&Route{Function: "delete", Uses: []string{"Authorization"}},
		},
		{
			"Use multiple",
			getFuncDecl(`
        // @Use Authorization
        // @Use getUser
        func (c *controller) delete() {}
      `),
			&Route{Function: "delete", Uses: []string{"Authorization", "getUser"}},
		},
		{
			"Complete post method",
			getFuncDecl(`
        // @Post
        // @Body product A product to add to the catalog
        func (c *controller) Add(product *Product){}
      `),
			&Route{Function: "Add", Method: Post, Parameters: []*Parameter{{Name: "product", Description: "A product to add to the catalog", Source: SourceBody, Position: 0, Type: "Product", IsRef: true}}},
		},
		{
			"Body missing description and route params",
			getFuncDecl(`
        // @Put /{id}
        // @Body product
        func (c *controller) Update(id int, product *Product){}
      `),
			&Route{Path: "/{id}", Function: "Update", Method: Put, Parameters: []*Parameter{{Name: "id", Source: SourcePath, Position: 0, Type: "int"}, {Name: "product", Description: "", Source: SourceBody, Position: 1, Type: "Product", IsRef: true}}},
		},
		{
			"Param documentation",
			getFuncDecl(`
        // @Get /{name}
        // @Param name Name to get history for
        func (c *controller) explain(name string){}
      `),
			&Route{Path: "/{name}", Function: "explain", Method: Get, Parameters: []*Parameter{{Name: "name", Description: "Name to get history for", Source: SourcePath, Position: 0, Type: "string"}}},
		},
		{
			"Param documentation before route",
			getFuncDecl(`
        // @Param name Name to get history for
        // @Param surname Surname to get history for
        // @Get /{name}/{surname}
        func (c *controller) explain(name string){}
      `),
			&Route{Path: "/{name}/{surname}", Function: "explain", Method: Get, Parameters: []*Parameter{{Name: "name", Description: "Name to get history for", Source: SourcePath, Position: 0, Type: "string"}, {Name: "surname", Description: "Surname to get history for", Source: SourcePath, Position: -1}}},
		},
		{
			"Param documentation - not it path",
			getFuncDecl(`
        // @Get /
        // @Param name Name to get history for
        func (c *controller) explain(name string){}
      `),
			&Route{Path: "/", Function: "explain", Method: Get, Parameters: []*Parameter{{Name: "name", Description: "Name to get history for", Position: 0, Type: "string"}}},
		},
		{
			"Query documentation",
			getFuncDecl(`
        // @Get /
        // @Query date Day to get logs for
        func (c *controller) fetch(date time.Time){}
      `),
			&Route{Path: "/", Function: "fetch", Method: Get, Parameters: []*Parameter{{Name: "date", Description: "Day to get logs for", Source: SourceQuery, Position: 0, Path: "time", Type: "Time"}}},
		},
	}

	for _, test := range cases {
		actual := parseFuncDecl(test.funcDecl)

		if !reflect.DeepEqual(actual, test.route) {
			t.Errorf("'%s' routes do not match: %s", test.description, diff(actual, test.route))
		}
	}
}

func getFuncDecl(x string) *ast.FuncDecl {
	x = fmt.Sprintf(`package test
	  type controller struct{}
	  %s
	  `, x)

	fset := token.NewFileSet()

	e, err := parser.ParseFile(fset, "", x, parser.ParseComments)

	if err != nil {
		panic(err)
	}

	return e.Decls[1].(*ast.FuncDecl)
}

func diff(actual, expected interface{}) string {
	spew.Config.SortKeys = true
	spew.Config.SpewKeys = true
	a := spew.Sdump(actual)
	b := spew.Sdump(expected)

	dmp := diffmatchpatch.New()
	diffs := dmp.DiffMain(b, a, false)

	return dmp.DiffPrettyText(diffs)
}
