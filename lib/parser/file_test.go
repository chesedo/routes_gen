package parser

import (
	"go/ast"
	"go/parser"
	"go/token"
	"reflect"
	"testing"
)

func Test_parseFile(t *testing.T) {
	cases := []struct {
		description string
		file        *ast.File
		controller  *Controller
	}{
		{"Empty file", getFile("package controllers"), nil},
		{
			"Only controller declaration",
			getFile(`
      package controllers

      type User struct {}
      `),
			&Controller{Controller: "User"},
		},
		{
			"With non-controller type",
			getFile(`
      package controllers

      type User struct {}
      type IUser interface {}
      `),
			&Controller{Controller: "User"},
		},
		{
			"Single method",
			getFile(`
      package controllers

      type User struct {}

      func (c User) Get() {}
      `),
			&Controller{Controller: "User", Routes: []*Route{{Function: "Get"}}},
		},
		{
			"With constructor",
			getFile(`
      package controllers

      type User struct {}

      func NewUser() User {}
      `),
			&Controller{Controller: "User", Injectors: []injector{}},
		},
		{
			"Single method and a function",
			getFile(`
      package controllers

      type User struct {}

      func (c User) Get() {}

      func help() {}
      `),
			&Controller{Controller: "User", Routes: []*Route{{Function: "Get"}}},
		},
		{
			"Complete CRUD",
			getFile(`
      package controllers

      // @Path /user
      // @Use Logger
      type User struct {}

      func NewUser(table string) User {}

      // @Get /{id}
      // @Param id The id of the user to get
      func (c User) Get(id string) {}

      // @Post /
      // @Body user The user to add
      func (c User) Add(user UserAddRequest) {}

      // @Put /{id}
      // @Param id Id of the user to update
      // @Body user The user to add
      // @Use Authentication
      func (c User) Update(id string, user *UserAddRequest) {}

      // @Delete /{id}
      // @Param id The id of the user to remove
      // @Use Authentication
      func (c User) Remove(id string) {}
      `),
			&Controller{
				Controller: "User",
				Injectors:  []injector{{name: "table", Type: "string"}},
				Path:       "/user",
				Uses:       []string{"Logger"},
				Routes: []*Route{
					{Function: "Get", Path: "/{id}", Method: Get, Parameters: []*Parameter{{Name: "id", Description: "The id of the user to get", Position: 0, Source: SourcePath, Type: "string"}}},
					{Function: "Add", Path: "/", Method: Post, Parameters: []*Parameter{{Name: "user", Description: "The user to add", Position: 0, Source: SourceBody, Type: "UserAddRequest"}}},
					{Function: "Update", Path: "/{id}", Method: Put, Parameters: []*Parameter{{Name: "id", Description: "Id of the user to update", Position: 0, Source: SourcePath, Type: "string"}, {Name: "user", Description: "The user to add", Position: 1, Source: SourceBody, Type: "UserAddRequest", IsRef: true}}, Uses: []string{"Authentication"}},
					{Function: "Remove", Path: "/{id}", Method: Delete, Parameters: []*Parameter{{Name: "id", Description: "The id of the user to remove", Position: 0, Source: SourcePath, Type: "string"}}, Uses: []string{"Authentication"}},
				},
			},
		},
	}

	for _, test := range cases {
		actual := parseFile(test.file)

		if !reflect.DeepEqual(actual, test.controller) {
			t.Errorf("'%s' controllers do not match: %s", test.description, diff(actual, test.controller))
		}
	}
}

func getFile(x string) *ast.File {
	fset := token.NewFileSet()

	e, err := parser.ParseFile(fset, "", x, parser.ParseComments)

	if err != nil {
		panic(err)
	}

	return e
}
