module gitlab.com/chesedo/routes_gen

go 1.16

require (
	github.com/dave/jennifer v1.4.1
	github.com/davecgh/go-spew v1.1.1
	github.com/sergi/go-diff v1.2.0
	golang.org/x/tools v0.1.5
)
